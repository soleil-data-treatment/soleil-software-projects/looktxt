# Makefile to compile looktxt.
# Required: gcc
# Optional: HDF5/NeXus library (libnexus)
#
# just type: make

ifeq ($(NEXUS_NAPI),)
  NEXUS_NAPI = $(shell [ -f /usr/include/napi.h ] && echo -DUSE_NEXUS -lNeXus || echo "")
endif

# simple one-shot compile
all:	
	${CC} -O2 -o looktxt looktxt.c $(NEXUS_NAPI) $(CFLAGS) $(LDFLAGS) $(CPPFLAGS)

help:
	@help2man --output=man/looktxt.1 --version-string=1.4.1 --no-discard-stderr --no-info --name="search and export numerics from any text/ascii file" ./looktxt || echo "WARNING: failed to generate man page"
	@pandoc README.md --to pdf > man/README.pdf || echo "WARNING: skip pdf doc (missing pandoc)"
	@echo "Usage: install looktxt"
	@echo "make all"
	@echo "  compile"
	@echo "make install"
	@echo "  install looktxt"
	@echo "make deb"
	@echo "  create debian package"

clean: 
	rm -f *.o looktxt

install:
	install -D looktxt       $(DESTDIR)$(prefix)/usr/bin/looktxt
	install -D man/looktxt.1 $(DESTDIR)$(prefix)/usr/share/man/man1/looktxt.1
	install -D -d $(DESTDIR)$(prefix)/usr/share/doc/looktxt
	install -D man/README.pdf $(DESTDIR)$(prefix)/usr/share/doc/looktxt

distclean: clean

test:
	./looktxt --version
	./looktxt --verbose --test examples/* 
	@echo Test OK

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/looktxt
	-rm -f $(DESTDIR)$(prefix)/usr/share/man/man1/looktxt.*
	-rm -fr $(DESTDIR)$(prefix)/usr/share/doc/looktxt
	
deb:
	if [ -d packaging/debian ]; then cp -r packaging/debian .; fi
	debuild -b || true
	rm -rf debian

.PHONY: all install clean distclean uninstall test deb

